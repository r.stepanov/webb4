<?php
$good=0;
$db = new PDO('mysql:host=localhost;dbname=u21139', 'u21139', '2017773', array(PDO::ATTR_PERSISTENT => true));

if(empty($_GET["radio1"])){
    if(!empty($_COOKIE["name"])){
        $name = $_COOKIE["name"];
        $password = "";
        $email = $_COOKIE["email"];
        $date = $_COOKIE["date"];
    }
    else{
        $name = "";
        $password="";
        $email = "";
        $date = "";
    }

    $error_check="";
    $error_date="";
    $error_password1="";
    $error_password2="";
    $error_email1="";
    $error_email2="";
    $error_name1="";
    $error_name2="";
    $check_gen1="checked";
    $check_gen2="";
    $check_lim = array("","","","","checked");
    $text="";
    $answer="";
    $answer_style="";
    

}
else{
    if(!empty($_GET["name"])){
    $name = htmlentities($_GET["name"]);
    $res = $db->prepare("SELECT name FROM info WHERE name='$name'");
    $res->execute();
    $db_name = $res->fetch(PDO::FETCH_NUM);
    if(!$db_name){
        $num = strlen($name);
        $f=1;
        $error_name1 = "";
        $error_name2 = "";
        for($i = 0;$i<$num;$i++){
            if(($name[$i]<"A" || ($name[$i]>"Z" && $name[$i]<"a") || $name[$i]>"z"))$f=0;
        }
        if($f==0) {
            setcookie("error_name","error");
            $error_name1 = " style='color: red;' ";
            $error_name2 = "(a-z or A-Z)";
        }
        else{
            $error_name1 = " style='color: rgb(32, 179, 32);' ";
            $good++;
        }
    }
    else{
        setcookie("error_name","error");
        $error_name1 = " style='color: red;' ";
        $error_name2 = "Имя уже занято";
    }
}
else {
    setcookie("error_name","error");
    $name = "";
    $error_name1 = " style='color: red;' ";
    $error_name2 = "Введите имя";
}

if(!empty($_GET["password"])){
    $password = htmlentities($_GET["password"]);
    $error_password1 = " style='color: rgb(32, 179, 32);' ";
    $error_password2 = "";
    $good++;
}
else{
    $password="";
    $error_password1 = " style='color: red;' ";
    $error_password2 = "Введите пароль";
}

if(!empty($_GET["email"])){
    $email = htmlentities($_GET["email"]);
    $num = strlen($email);
    $f=0;
    $error_email1="";
    $error_email2="";
    for($i=0;$i<$num;$i++){
        if($email[$i]=="@" && $i>0 && $i<$num-1)$f++;
    }
    if($f!=1){
        setcookie("error_email","error");
        $error_email1=" style='color: red;' ";
        $error_email2 = "( ___@___ )";
    }
    else{
        $error_email1 = " style='color: rgb(32, 179, 32);' ";
        $good++;
    }
}
else{
    setcookie("error_email","error");
    $email = "";
    $error_email1=" style='color: red;' ";
    $error_email2 = "( ___@___ )";
}

if(!empty($_GET["date"])){
    $date = htmlentities($_GET["date"]);
    $error_date = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    setcookie("error_date","error");
    $date = "";
    $error_date = " style='color: red;' ";
}


if(!empty($_GET["radio1"])){
    $gen = $_GET["radio1"];
    if($gen=="m"){
        $check_gen1="checked";
        $check_gen2="";
    }
    else{
        $check_gen1="";
        $check_gen2="checked";
    }
}else{
    $check_gen1="checked";
    $check_gen2="";
}

if(!empty($_GET["radio2"])){
    $lim = $_GET["radio2"];
    $check_lim = array("","","","","");
    for($i=1;$i<=4;$i++){
        if($i==$lim)$check_lim[$i]="checked";
        else $check_lim[$i]="";
    }
}else{
    $check_lim = array("","","","checked");
}

if(!empty($_GET["textarea1"])){
    $text = $_GET["textarea1"];
}else{
    $text = "";
}

$sel="0";
if(isset($_GET["select"])){
    $sel=" ";
    foreach ($_GET["select"] as $valve){
    $sel .= $valve;
}
$sel = (int)$sel;
}

if(!empty($_GET["checkbox"])){
    $error_check = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    $error_check=" style='color: red;' ";
}



if($good==5){
    $answer_style =" style='color: rgb(32, 179, 32);' ";
    $answer = "data sent";
    
    $stmt = $db->prepare("INSERT INTO info (name, password, email, date, gender, limbs, power_code, biography) VALUES (:f_name, :f_password, :f_email, :f_date, :f_gen, :f_lim, :f_powers, :f_biograp)");
    $stmt->bindParam(':f_name', $name);
    $stmt->bindParam(':f_password', $password);
    $stmt->bindParam(':f_email', $email);
    $stmt->bindParam(':f_date', $date);
    $stmt->bindParam(':f_gen', $gen);
    $stmt->bindParam(':f_lim', $lim);
    $stmt->bindParam(':f_powers', $sel);
    $stmt->bindParam(':f_biograp', $text);
    $stmt->execute();

    $time = 60*60*24*365;


    setcookie("name", $name, time()+$time);
    setcookie("email", $email, time()+$time);
    setcookie("date", $date, time()+$time);

    setcookie("error_name"," ",1);
    setcookie("error_email"," ",1);
    setcookie("error_date"," ",1);
}
else{
    $answer_style =" style='color: red;' ";
    $answer = "Исправте ошибки";
}
}



$max = $db->prepare("SELECT MAX(id)FROM info");
$max->execute();
$db_max = $max->fetch(PDO::FETCH_NUM);
$options = "";
for($i=1;$i<=$db_max[0];$i++){
    $look = $db->prepare("SELECT name FROM info WHERE id = $i");
    $look->execute();
    $db_names = $look->fetch(PDO::FETCH_NUM);
    $options = $options . '<option value="'.$i.'">'.$db_names[0].'</option>';
}



$result='<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>MY site</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>
    </script>
</head>
<body>
    <div id="main">
        <div class="window_1" id="button1">Загрузить данные</div>
        <div class="window_2" id="button2">Показать данные</div>
    </div>
    <div id="forms">
        <div class="window_1_1" id="first_form">
            <div class="line"></div>
            <div>
                <form id="form" method="GET" action="form.php">
                    <label' . $error_name1 . '>Имя: 
                        <input  id="name" name="name" value=' . $name. '><br>'. $error_name2 .'
                    </label><br><br>

                    <label'.$error_password1.'>Пароль: 
                        <input  id="password" name="password" value='.$password.'><br>'.$error_password2.'
                    </label><br><br>
    
                    <label' . $error_email1 . '>E-mail:
                        <input  type="text" id="email" name="email" value=' . $email .'><br>'. $error_email2 .'
                    </label><br><br>

                    <lable' . $error_date . '>Дата рождения:
                        <input type="date" name="date" value=' . $date . '><br><br>
                    </lable>
                    
                    Пол:
                    <label> Мужчина
                        <input name="radio1" type="radio" value="m"'.$check_gen1.'>
                    </label>
                    <label> Женщина
                        <input name="radio1" type="radio" value="w"'.$check_gen2.'>
                    </label><br><br>
    
                    Кол-во конечностей:
                    <label><input name="radio2" type="radio" value="1"'.$check_lim[1].'> 1</label>
                    <label><input name="radio2" type="radio" value="2"'.$check_lim[2].'> 2</label>
                    <label><input name="radio2" type="radio" value="3"'.$check_lim[3].'> 3</label>
                    <label><input name="radio2" type="radio" value="4"'.$check_lim[4].'> 4</label><br><br>
                    
                    <label>Сверхспособности:<br>
                        <select name="select[]" size="3" multiple>
                            <option value="1">immortality</option>
                            <option value="2">passing through walls</option>
                            <option value="3">levitation</option>
                        </select>
                    </label><br><br>
                    
                    <label>Биография:<br>
                        <textarea cols="40" rows="3" name="textarea1">'.$text.'</textarea>
                    </label><br><br>
                    
                    <label ><div'. $error_check .'>Я согласен со всем:</div>
                        <input id="checkbox" name="checkbox" type="checkbox" value="ok">
                    </label><br><br>
                    
                    <input type="submit" value="Отправить"><br><br>
                    <div'. $answer_style .'>' . $answer . '</div>
                </form>
    
            </div>
        </div>
        <div class="window_2_1" id="second_form">
            <div class="line"></div>
            <div>
                <form id="form1"  method="GET" action="form1.php">

                    <label>Имена:<br>
                        <select name="select1[]" size="6">
                            '.$options.'
                        </select>
                    </label><br>

                    <input type="submit" value="Получить"><br>
                </form>
            </div>
        </div>
        <div class="window_2_2">
            Изменить данные
        </div>
        <div class="window_2_3">
            <div class="line"></div>
            <div class="change">
                <div class="change_1">
                    <form id="form2"  method="GET" action="form2.php">

                    <label>Имя: 
                        <input  id="name" name="name">
                    </label><br><br>

                    <label>Пароль: 
                        <input  id="password" name="password"><br>
                    </label><br><br>

                    <input type="submit" value="Войти"><br>
                </form>
                </div>
                <div class="change_2">
                    <form id="form3"  method="GET" action="form3.php" style="display: none;">  

                    <label>E-mail:
                        <input  type="text" id="email" name="email"><br>
                    </label><br>

                    <lable>Дата рождения:
                        <input type="date" name="date"><br><br>
                    </lable>
                    
                    Пол:
                    <label> Мужчина
                        <input name="radio1" type="radio" value="m" checked>
                    </label>
                    <label> Женщина
                        <input name="radio1" type="radio" value="w">
                    </label><br><br>
    
                    Кол-во конечностей:
                    <label><input name="radio2" type="radio" value="1"> 1</label>
                    <label><input name="radio2" type="radio" value="2"> 2</label>
                    <label><input name="radio2" type="radio" value="3"> 3</label>
                    <label><input name="radio2" type="radio" value="4" checked> 4</label><br><br>
                    
                    <label>Сверхспособности:<br>
                        <select name="select[]" size="3" multiple>
                            <option value="1">immortality</option>
                            <option value="2">passing through walls</option>
                            <option value="3">levitation</option>
                        </select>
                    </label><br><br>
                    
                    <label>Биография:<br>
                        <textarea cols="40" rows="3" name="textarea1"></textarea>
                    </label><br><br>

                    <input type="submit" value="Изменить"><br>

                    </form>    
                </div>
            </div>
        </div>
    </div>
</body>
</html>';

echo "$result";
?>
